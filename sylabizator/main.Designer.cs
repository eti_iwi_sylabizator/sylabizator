﻿namespace sylabizator
{
    partial class main
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.programToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.loadTextToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.loadSampleTextToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.provideTextToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.clearWordsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem2 = new System.Windows.Forms.ToolStripSeparator();
            this.fileEncodingToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.detectToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.aNSIToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.uTF8ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.iSO88592ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.moreToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.changeLanguageToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem1 = new System.Windows.Forms.ToolStripSeparator();
            this.closeToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.statisticsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.generateToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.validateOnlineToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.findWordToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.saveWordsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.loadWordsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.drawGraphToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.showAnalyzerToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.helpToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.authorsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.statusStrip1 = new System.Windows.Forms.StatusStrip();
            this.languageStatus = new System.Windows.Forms.ToolStripStatusLabel();
            this.statStatus = new System.Windows.Forms.ToolStripStatusLabel();
            this.progressBar = new System.Windows.Forms.ToolStripProgressBar();
            this.progresStatus = new System.Windows.Forms.ToolStripStatusLabel();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.menuStrip1.SuspendLayout();
            this.statusStrip1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            this.SuspendLayout();
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.programToolStripMenuItem,
            this.statisticsToolStripMenuItem,
            this.helpToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(542, 24);
            this.menuStrip1.TabIndex = 2;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // programToolStripMenuItem
            // 
            this.programToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.loadTextToolStripMenuItem,
            this.loadSampleTextToolStripMenuItem,
            this.provideTextToolStripMenuItem,
            this.clearWordsToolStripMenuItem,
            this.toolStripMenuItem2,
            this.fileEncodingToolStripMenuItem,
            this.changeLanguageToolStripMenuItem,
            this.toolStripMenuItem1,
            this.closeToolStripMenuItem});
            this.programToolStripMenuItem.Name = "programToolStripMenuItem";
            this.programToolStripMenuItem.Size = new System.Drawing.Size(59, 20);
            this.programToolStripMenuItem.Text = "Program";
            // 
            // loadTextToolStripMenuItem
            // 
            this.loadTextToolStripMenuItem.Image = global::sylabizator.Properties.Resources.folder_page;
            this.loadTextToolStripMenuItem.Name = "loadTextToolStripMenuItem";
            this.loadTextToolStripMenuItem.Size = new System.Drawing.Size(158, 22);
            this.loadTextToolStripMenuItem.Text = "Open text file";
            this.loadTextToolStripMenuItem.Click += new System.EventHandler(this.loadTextToolStripMenuItem_Click);
            // 
            // loadSampleTextToolStripMenuItem
            // 
            this.loadSampleTextToolStripMenuItem.Image = global::sylabizator.Properties.Resources.text_allcaps;
            this.loadSampleTextToolStripMenuItem.Name = "loadSampleTextToolStripMenuItem";
            this.loadSampleTextToolStripMenuItem.Size = new System.Drawing.Size(158, 22);
            this.loadSampleTextToolStripMenuItem.Text = "Load sample text";
            this.loadSampleTextToolStripMenuItem.DropDownOpening += new System.EventHandler(this.loadSampleTextToolStripMenuItem_DropDownOpening);
            // 
            // provideTextToolStripMenuItem
            // 
            this.provideTextToolStripMenuItem.Name = "provideTextToolStripMenuItem";
            this.provideTextToolStripMenuItem.Size = new System.Drawing.Size(158, 22);
            this.provideTextToolStripMenuItem.Text = "Provide text";
            this.provideTextToolStripMenuItem.Click += new System.EventHandler(this.provideTextToolStripMenuItem_Click);
            // 
            // clearWordsToolStripMenuItem
            // 
            this.clearWordsToolStripMenuItem.Name = "clearWordsToolStripMenuItem";
            this.clearWordsToolStripMenuItem.Size = new System.Drawing.Size(158, 22);
            this.clearWordsToolStripMenuItem.Text = "Clear words";
            this.clearWordsToolStripMenuItem.Click += new System.EventHandler(this.clearWordsToolStripMenuItem_Click);
            // 
            // toolStripMenuItem2
            // 
            this.toolStripMenuItem2.Name = "toolStripMenuItem2";
            this.toolStripMenuItem2.Size = new System.Drawing.Size(155, 6);
            // 
            // fileEncodingToolStripMenuItem
            // 
            this.fileEncodingToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.detectToolStripMenuItem,
            this.aNSIToolStripMenuItem,
            this.uTF8ToolStripMenuItem,
            this.iSO88592ToolStripMenuItem,
            this.moreToolStripMenuItem});
            this.fileEncodingToolStripMenuItem.Name = "fileEncodingToolStripMenuItem";
            this.fileEncodingToolStripMenuItem.Size = new System.Drawing.Size(158, 22);
            this.fileEncodingToolStripMenuItem.Text = "File encoding";
            // 
            // detectToolStripMenuItem
            // 
            this.detectToolStripMenuItem.Checked = true;
            this.detectToolStripMenuItem.CheckState = System.Windows.Forms.CheckState.Checked;
            this.detectToolStripMenuItem.Name = "detectToolStripMenuItem";
            this.detectToolStripMenuItem.Size = new System.Drawing.Size(129, 22);
            this.detectToolStripMenuItem.Text = "Detect";
            this.detectToolStripMenuItem.Click += new System.EventHandler(this.encodingSelected);
            // 
            // aNSIToolStripMenuItem
            // 
            this.aNSIToolStripMenuItem.Name = "aNSIToolStripMenuItem";
            this.aNSIToolStripMenuItem.Size = new System.Drawing.Size(129, 22);
            this.aNSIToolStripMenuItem.Text = "ANSI";
            this.aNSIToolStripMenuItem.Click += new System.EventHandler(this.encodingSelected);
            // 
            // uTF8ToolStripMenuItem
            // 
            this.uTF8ToolStripMenuItem.Name = "uTF8ToolStripMenuItem";
            this.uTF8ToolStripMenuItem.Size = new System.Drawing.Size(129, 22);
            this.uTF8ToolStripMenuItem.Text = "UTF-8";
            this.uTF8ToolStripMenuItem.Click += new System.EventHandler(this.encodingSelected);
            // 
            // iSO88592ToolStripMenuItem
            // 
            this.iSO88592ToolStripMenuItem.Name = "iSO88592ToolStripMenuItem";
            this.iSO88592ToolStripMenuItem.Size = new System.Drawing.Size(129, 22);
            this.iSO88592ToolStripMenuItem.Text = "ISO 8859-2";
            this.iSO88592ToolStripMenuItem.Click += new System.EventHandler(this.encodingSelected);
            // 
            // moreToolStripMenuItem
            // 
            this.moreToolStripMenuItem.Name = "moreToolStripMenuItem";
            this.moreToolStripMenuItem.Size = new System.Drawing.Size(129, 22);
            this.moreToolStripMenuItem.Text = "More...";
            this.moreToolStripMenuItem.Click += new System.EventHandler(this.encodingSelected);
            // 
            // changeLanguageToolStripMenuItem
            // 
            this.changeLanguageToolStripMenuItem.Name = "changeLanguageToolStripMenuItem";
            this.changeLanguageToolStripMenuItem.Size = new System.Drawing.Size(158, 22);
            this.changeLanguageToolStripMenuItem.Text = "Change language";
            this.changeLanguageToolStripMenuItem.Click += new System.EventHandler(this.changeLanguageToolStripMenuItem_Click);
            // 
            // toolStripMenuItem1
            // 
            this.toolStripMenuItem1.Name = "toolStripMenuItem1";
            this.toolStripMenuItem1.Size = new System.Drawing.Size(155, 6);
            // 
            // closeToolStripMenuItem
            // 
            this.closeToolStripMenuItem.Image = global::sylabizator.Properties.Resources.application_go;
            this.closeToolStripMenuItem.Name = "closeToolStripMenuItem";
            this.closeToolStripMenuItem.Size = new System.Drawing.Size(158, 22);
            this.closeToolStripMenuItem.Text = "Close";
            this.closeToolStripMenuItem.Click += new System.EventHandler(this.closeToolStripMenuItem_Click);
            // 
            // statisticsToolStripMenuItem
            // 
            this.statisticsToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.generateToolStripMenuItem,
            this.toolStripSeparator1,
            this.validateOnlineToolStripMenuItem,
            this.findWordToolStripMenuItem,
            this.saveWordsToolStripMenuItem,
            this.loadWordsToolStripMenuItem,
            this.toolStripSeparator2,
            this.drawGraphToolStripMenuItem,
            this.showAnalyzerToolStripMenuItem});
            this.statisticsToolStripMenuItem.Name = "statisticsToolStripMenuItem";
            this.statisticsToolStripMenuItem.Size = new System.Drawing.Size(60, 20);
            this.statisticsToolStripMenuItem.Text = "Syllables";
            // 
            // generateToolStripMenuItem
            // 
            this.generateToolStripMenuItem.Name = "generateToolStripMenuItem";
            this.generateToolStripMenuItem.Size = new System.Drawing.Size(160, 22);
            this.generateToolStripMenuItem.Text = "Generate";
            this.generateToolStripMenuItem.Click += new System.EventHandler(this.generateToolStripMenuItem_Click);
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(157, 6);
            // 
            // validateOnlineToolStripMenuItem
            // 
            this.validateOnlineToolStripMenuItem.Name = "validateOnlineToolStripMenuItem";
            this.validateOnlineToolStripMenuItem.Size = new System.Drawing.Size(160, 22);
            this.validateOnlineToolStripMenuItem.Text = "Validate online";
            this.validateOnlineToolStripMenuItem.Click += new System.EventHandler(this.validateOnlineToolStripMenuItem_Click);
            // 
            // findWordToolStripMenuItem
            // 
            this.findWordToolStripMenuItem.CheckOnClick = true;
            this.findWordToolStripMenuItem.Name = "findWordToolStripMenuItem";
            this.findWordToolStripMenuItem.Size = new System.Drawing.Size(160, 22);
            this.findWordToolStripMenuItem.Text = "Find word";
            this.findWordToolStripMenuItem.Click += new System.EventHandler(this.findWordToolStripMenuItem_Click);
            // 
            // saveWordsToolStripMenuItem
            // 
            this.saveWordsToolStripMenuItem.Name = "saveWordsToolStripMenuItem";
            this.saveWordsToolStripMenuItem.Size = new System.Drawing.Size(160, 22);
            this.saveWordsToolStripMenuItem.Text = "Save dictionary...";
            this.saveWordsToolStripMenuItem.Click += new System.EventHandler(this.saveWordsToolStripMenuItem_Click);
            // 
            // loadWordsToolStripMenuItem
            // 
            this.loadWordsToolStripMenuItem.Name = "loadWordsToolStripMenuItem";
            this.loadWordsToolStripMenuItem.Size = new System.Drawing.Size(160, 22);
            this.loadWordsToolStripMenuItem.Text = "Load dictionary...";
            this.loadWordsToolStripMenuItem.Click += new System.EventHandler(this.loadWordsToolStripMenuItem_Click);
            // 
            // toolStripSeparator2
            // 
            this.toolStripSeparator2.Name = "toolStripSeparator2";
            this.toolStripSeparator2.Size = new System.Drawing.Size(157, 6);
            // 
            // drawGraphToolStripMenuItem
            // 
            this.drawGraphToolStripMenuItem.Name = "drawGraphToolStripMenuItem";
            this.drawGraphToolStripMenuItem.Size = new System.Drawing.Size(160, 22);
            this.drawGraphToolStripMenuItem.Text = "Draw graph";
            this.drawGraphToolStripMenuItem.Click += new System.EventHandler(this.drawGraphToolStripMenuItem_Click);
            // 
            // showAnalyzerToolStripMenuItem
            // 
            this.showAnalyzerToolStripMenuItem.Name = "showAnalyzerToolStripMenuItem";
            this.showAnalyzerToolStripMenuItem.Size = new System.Drawing.Size(160, 22);
            this.showAnalyzerToolStripMenuItem.Text = "Show analyzer";
            this.showAnalyzerToolStripMenuItem.Click += new System.EventHandler(this.showAnalyzerToolStripMenuItem_Click);
            // 
            // helpToolStripMenuItem
            // 
            this.helpToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.authorsToolStripMenuItem});
            this.helpToolStripMenuItem.Name = "helpToolStripMenuItem";
            this.helpToolStripMenuItem.Size = new System.Drawing.Size(40, 20);
            this.helpToolStripMenuItem.Text = "Help";
            // 
            // authorsToolStripMenuItem
            // 
            this.authorsToolStripMenuItem.Name = "authorsToolStripMenuItem";
            this.authorsToolStripMenuItem.Size = new System.Drawing.Size(112, 22);
            this.authorsToolStripMenuItem.Text = "Authors";
            this.authorsToolStripMenuItem.Click += new System.EventHandler(this.authorsToolStripMenuItem_Click);
            // 
            // statusStrip1
            // 
            this.statusStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.languageStatus,
            this.statStatus,
            this.progressBar,
            this.progresStatus});
            this.statusStrip1.Location = new System.Drawing.Point(0, 337);
            this.statusStrip1.Name = "statusStrip1";
            this.statusStrip1.Size = new System.Drawing.Size(542, 22);
            this.statusStrip1.TabIndex = 3;
            this.statusStrip1.Text = "statusStrip1";
            // 
            // languageStatus
            // 
            this.languageStatus.Name = "languageStatus";
            this.languageStatus.Size = new System.Drawing.Size(58, 17);
            this.languageStatus.Text = "Language:";
            // 
            // statStatus
            // 
            this.statStatus.Name = "statStatus";
            this.statStatus.Size = new System.Drawing.Size(102, 17);
            this.statStatus.Text = "Statistic: 0 / 0 (0%)";
            // 
            // progressBar
            // 
            this.progressBar.Name = "progressBar";
            this.progressBar.Size = new System.Drawing.Size(100, 16);
            // 
            // progresStatus
            // 
            this.progresStatus.Name = "progresStatus";
            this.progresStatus.Size = new System.Drawing.Size(53, 17);
            this.progresStatus.Text = "Progress:";
            // 
            // dataGridView1
            // 
            this.dataGridView1.AllowUserToAddRows = false;
            this.dataGridView1.AllowUserToDeleteRows = false;
            this.dataGridView1.AllowUserToResizeRows = false;
            this.dataGridView1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dataGridView1.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.dataGridView1.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.Single;
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
            this.dataGridView1.Location = new System.Drawing.Point(12, 27);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.RowHeadersVisible = false;
            this.dataGridView1.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dataGridView1.Size = new System.Drawing.Size(518, 296);
            this.dataGridView1.TabIndex = 4;
            this.dataGridView1.CellFormatting += new System.Windows.Forms.DataGridViewCellFormattingEventHandler(this.dataGridView1_CellFormatting);
            this.dataGridView1.CellValidating += new System.Windows.Forms.DataGridViewCellValidatingEventHandler(this.dataGridView1_CellValidating);
            this.dataGridView1.DataError += new System.Windows.Forms.DataGridViewDataErrorEventHandler(this.dataGridView1_DataError);
            // 
            // main
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(542, 359);
            this.Controls.Add(this.dataGridView1);
            this.Controls.Add(this.statusStrip1);
            this.Controls.Add(this.menuStrip1);
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "main";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Hyphenator";
            this.Load += new System.EventHandler(this.main_Load);
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.statusStrip1.ResumeLayout(false);
            this.statusStrip1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem programToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem changeLanguageToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem closeToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem statisticsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem helpToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem authorsToolStripMenuItem;
        private System.Windows.Forms.StatusStrip statusStrip1;
        private System.Windows.Forms.ToolStripStatusLabel languageStatus;
        private System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.ToolStripMenuItem loadTextToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem loadSampleTextToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripMenuItem2;
        private System.Windows.Forms.ToolStripMenuItem validateOnlineToolStripMenuItem;
        private System.Windows.Forms.ToolStripStatusLabel progresStatus;
        private System.Windows.Forms.ToolStripProgressBar progressBar;
        private System.Windows.Forms.ToolStripStatusLabel statStatus;
        private System.Windows.Forms.ToolStripMenuItem generateToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem provideTextToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem clearWordsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem findWordToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem saveWordsToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.ToolStripMenuItem loadWordsToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator2;
        private System.Windows.Forms.ToolStripMenuItem drawGraphToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem fileEncodingToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem detectToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem aNSIToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem uTF8ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem iSO88592ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem moreToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem showAnalyzerToolStripMenuItem;
    }
}

