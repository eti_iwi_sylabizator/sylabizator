﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace sylabizator
{
    public partial class EncodingChooser : Form
    {
        private bool apply = false;

        public EncodingChooser(Encoding currentSelection = null)
        {
            if (currentSelection == null)
            {
                currentSelection = Encoding.UTF8;
            }
            InitializeComponent();
            comboBox1.DropDownStyle = ComboBoxStyle.DropDownList;
            foreach (EncodingInfo enc in Encoding.GetEncodings())
            {
                int pos = comboBox1.Items.Add(new EncodingSelection(enc));
                if (enc.GetEncoding() == currentSelection)
                {
                    comboBox1.SelectedIndex = pos;
                }
            }
        }

        public Encoding GetChoosenEncoding()
        {
            this.ShowDialog();
            comboBox1.Focus();
            if (apply && comboBox1.SelectedItem != null && comboBox1.SelectedItem is EncodingSelection)
            {
                return (comboBox1.SelectedItem as EncodingSelection).encoding.GetEncoding();
            }
            else
            {
                return null;
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            apply = true;
            this.Close();
        }
    }

    public class EncodingSelection
    {
        public EncodingInfo encoding;

        public EncodingSelection(EncodingInfo encoding)
        {
            this.encoding = encoding;
        }

        public override string ToString()
        {
            return encoding.Name + ": " + encoding.DisplayName;
        }
    }
}
