﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace sylabizator
{
    public class Validator
    {
        private HyphenationLanguage lang;

        public Validator(HyphenationLanguage lang)
        {
            this.lang = lang;
        }

        private string getHttp(string url)
        {
            WebClient client = new WebClient();
            try
            {
                    //System.Text.Encoding.UTF8.GetString
                return client.DownloadString(url);
            }
            catch (WebException HTTP_Exception)
            {
                return "";
            }
        }

        public string[] RandomHypenations()
        {
            string url = WiktionaryRandomUrl();
            return HypenationsFromUrl(url);
        }

        public string getHypenation(string word)
        {
            string url = WiktionaryUrl(word);
            return ValidateWithWord(HypenationsFromUrl(url), word);
        }

        private string ValidateWithWord(string[] hyphs, string word)
        {
            foreach (string hyph in hyphs)
            {
                if (hyph.Replace(" - ", "") == word)
                {
                    return hyph;
                }
            }
            return "";
        }

        public string[] HypenationsFromUrl(string url)
        {
            string src = this.getHttp(url);

            Regex regex = new Regex(lang.WiktionaryBaseRegexp(), RegexOptions.IgnoreCase);
            MatchCollection matches = regex.Matches(src);
            List<string> hyphs = new List<string>();
            foreach (Match match in matches)
            {
                hyphs.Add(match.Groups[1].Value.Replace("|", " - ").ToLower());
            }
            return hyphs.ToArray();
        }

        private string WiktionaryRandomUrl()
        {
            return String.Format(
                "http://{0}.wiktionary.org/w/api.php?format=json&action=query&generator=random&grnlimit=500&grnnamespace=0&rvprop=content&prop=revisions&redirects=1",
                lang.domain);
        }

        private string WiktionaryUrl(string word)
        {
            return String.Format(
                "http://{0}.wiktionary.org/w/api.php?format=json&action=query&titles={1}&rvprop=content&prop=revisions&redirects=1",
                lang.domain, word);
        }

    }
}
