﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace sylabizator
{
    public class Hyphenator
    {
        private int leftMin = 1;
        private int rightMin = 1;

        private Dictionary<string, int[]> patterns = new Dictionary<string, int[]>();
        private Dictionary<string, int[]> exceptions = new Dictionary<string, int[]>();
        private static char[] whitespace = { ' ', '\n', '\r' };

        public Hyphenator(string patternsString, string exeptionsString, int leftMin, int rightMin) 
            : this(
                  patternsString.Split(whitespace, StringSplitOptions.RemoveEmptyEntries),
                  exeptionsString.Split(whitespace, StringSplitOptions.RemoveEmptyEntries),
                  leftMin, rightMin)
        {
        }

        public Hyphenator(string[] patternsArray, string[] exeptionsArray, int leftMin, int rightMin)
        {
            AddPatterns(patternsArray);
            AddExceptions(exeptionsArray);
            this.leftMin = leftMin;
            this.rightMin = rightMin;
        }

        public Hyphenator()
        {
        }

        public string HyphenateWord(string originalWord, string hyphenateSymbol)
        {
            string word = originalWord.ToLowerInvariant();
            int[] splits = exceptions.ContainsKey(word) ? exceptions[word] : SplitsForWord(word);
            return ApplyHyphenation(originalWord, splits, hyphenateSymbol);
        }

        private int[] SplitsForWord(string word)
        {
            string wordString = '.' + word + '.';
            var splits = new int[wordString.Length + 1];
            for (int start = 0; start < wordString.Length - 1; start++)
            {
                for (int len = 1; len <= wordString.Length - start; len++)
                {
                    var pattern = wordString.Substring(start, len);
                    if (patterns.ContainsKey(pattern))
                    {
                        for (int i = 0; i < patterns[pattern].Length; i++)
                        {
                            int level = patterns[pattern][i];
                            if (level > splits[start + i])
                            {
                                splits[start + i] = level;
                            }
                        }
                    }
                }
            }
            return splits.Skip(1).Take(splits.Length - 2).ToArray(); // no dots
        }

        private string ApplyHyphenation(string originalWord, int[] splits, string hyphenateSymbol)
        {
            if (originalWord.Length == 0)
                return "";

            var result = new StringBuilder();
            result.Append(originalWord[0]);
            for (int i = 1; i < originalWord.Length; i++)
            {
                if (i >= leftMin && 
                    i + rightMin <= originalWord.Length && 
                    splits[i] % 2 != 0)
                {
                    result.Append(hyphenateSymbol);
                }
                result.Append(originalWord[i]);
            }
            return result.ToString();
        }

        public void AddPatternsString(string patternsString)
        {
            AddPatterns(patternsString.Split(whitespace, StringSplitOptions.RemoveEmptyEntries));
        }

        public void AddPatterns(string[] patternArray)
        {
            foreach (string pattern in patternArray)
            {
                AddPattern(pattern);
            }
        }

        public void AddPattern(string pattern)
        {
            var splits = new List<int>(pattern.Length + 1);
            StringBuilder patternKey = new StringBuilder();
            bool foundSplit = false;
            foreach (char character in pattern)
            {
                if (char.IsDigit(character))
                {
                    splits.Add(int.Parse(character.ToString()));
                    foundSplit = true;
                }
                else
                {
                    if (!foundSplit)
                    {
                        splits.Add(0);
                    }
                    patternKey.Append(character);
                    foundSplit = false;
                }
            }

            if (!foundSplit)
            {
                splits.Add(0);
            }

            patterns[patternKey.ToString()] = splits.ToArray();
        }

        public void AddExceptions(string[] exeptionsArray)
        {
            foreach (string exeption in exeptionsArray)
            {
                AddException(exeption);
            }
        }

        public void AddException(string exeption)
        {
            var splits = new List<int>(exeption.Length + 1);
            StringBuilder exeptionKey = new StringBuilder();
            bool foundSplit = false;
            foreach (char character in exeption)
            {
                if (character == '-')
                {
                    splits.Add(1);
                    foundSplit = true;
                }
                else
                {
                    if (!foundSplit)
                    {
                        splits.Add(0);
                    }
                    exeptionKey.Append(character);
                    foundSplit = false;
                }
            }

            if (!foundSplit)
            {
                splits.Add(0);
            }

            patterns[exeptionKey.ToString()] = splits.ToArray();
        }
    }
}
