﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace sylabizator
{
    public partial class main : Form
    {
        public main()
        {
            InitializeComponent();
        }

        private HyphenationModel model = new HyphenationModel();

        private FileLoader fileLoader = new FileLoader();

        private ActionQueue actionQueue = new ActionQueue();

        private Dictionary<ToolStripMenuItem, Encoding> encodingMenuMapping = new Dictionary<ToolStripMenuItem, Encoding>();

        private int findersStarted = 0;

        private int ColumnWidth(string columnName)
        {
            switch (columnName)
            {
                case HyphenationModel.wordColumn: return 40;
                case HyphenationModel.countColumn: return 12;
                case HyphenationModel.syllableColumn: return 45;
                case HyphenationModel.dictionaryColumn: return 50;
            }
            return 1;
        }

        private void main_Load(object sender, EventArgs e)
        {
            model.lanugageChanged += ChangeLanguageText;
            model.statisticsChanged += ChangeStatisticsText;
            model.ChangeLanguage(
                HyphenationLanguage.IsSaved() ? HyphenationLanguage.LoadFromProperties() : new langForm().selectLang()
            );

            ReloadSampleTextMenu();

            /* Clear progress */
            progresStatus.Text = "";
            progressBar.Value = 0;

            InitializeDataGridView();

            InitializeContextMenu();

            /* Menu items mapping for encoding values */
            SafeBindMenuItemToEncoding(detectToolStripMenuItem, null);
            SafeBindMenuItemToEncoding(uTF8ToolStripMenuItem, Encoding.UTF8);
            SafeBindMenuItemToEncoding(aNSIToolStripMenuItem, Encoding.Default);
            SafeBindMenuItemToEncoding(iSO88592ToolStripMenuItem, Encoding.GetEncoding("iso-8859-2"));
        }

        private void InitializeDataGridView()
        {
            this.dataGridView1.DataSource = model.dataTable;

            int columnSizeSum = 0;
            foreach (DataColumn column in model.dataTable.Columns)
            {
                columnSizeSum += ColumnWidth(column.ColumnName);
            }
            foreach (DataColumn column in model.dataTable.Columns)
            {
                DataGridViewColumn gridColumn = this.dataGridView1.Columns[column.ColumnName];
                gridColumn.ReadOnly = true;
                gridColumn.Width = this.dataGridView1.Width * ColumnWidth(column.ColumnName) / columnSizeSum;
            }
            this.dataGridView1.Columns[this.dataGridView1.Columns.Count - 1].AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;

            this.dataGridView1.Columns[HyphenationModel.dictionaryColumn].ReadOnly = false;
        }

        private void InitializeContextMenu()
        {
            ContextMenuStrip contextManu = new ContextMenuStrip();

            ToolStripMenuItem markAsValidSelected = new ToolStripMenuItem("Mark selection as valid");
            markAsValidSelected.Click += MarkAsValidSelected_Click;
            contextManu.Items.Add(markAsValidSelected);

            ToolStripMenuItem generateFromSelected = new ToolStripMenuItem("Generate syllables for selection");
            generateFromSelected.Click += GenerateFromSelected_Click;
            contextManu.Items.Add(generateFromSelected);

            ToolStripMenuItem validateSelected = new ToolStripMenuItem("Validate online for selection");
            validateSelected.Click += ValidateSelected_Click;
            contextManu.Items.Add(validateSelected);

            ToolStripMenuItem graphFromSelected = new ToolStripMenuItem("Draw graph for selection");
            graphFromSelected.Click += GraphFromSelected_Click;
            contextManu.Items.Add(graphFromSelected);

            this.dataGridView1.ContextMenuStrip = contextManu;
        }

        private void SafeBindMenuItemToEncoding(ToolStripMenuItem encodingMenuItem, Encoding encoding)
        {
            try
            {
                encodingMenuMapping.Add(encodingMenuItem, encoding);
            }
            catch (Exception)
            {
                encodingMenuItem.Enabled = false;
            }
        }

        private void ReloadSampleTextMenu()
        {
            /* Fill menu entry 'Load sample text' with available samples */
            string[] filePaths = Directory.GetFiles(Application.StartupPath + @"\examples");

            foreach (ToolStripMenuItem item in loadSampleTextToolStripMenuItem.DropDownItems)
            {
                item.Click -= OpenExampleFileHandler;
            }

            loadSampleTextToolStripMenuItem.DropDownItems.Clear();

            foreach (string path in filePaths)
            {
                ToolStripMenuItem m = new ToolStripMenuItem();
                m.Text = Path.GetFileName(path).Replace(".txt", "");
                m.Click += OpenExampleFileHandler;

                /* Save dynamic position */
                loadSampleTextToolStripMenuItem.DropDownItems.Add(m);
            }
        }

        private void processText(string text)
        {
            actionQueue.QueueTask((Action)delegate
            {
                string[] words = Regex.Matches(text, @"(\b[^\s]+\b)").Cast<Match>().Select(m => m.Value).ToArray();

                BeginInvoke((Action)delegate
                {
                    progresStatus.Text = "Reading words...";
                    progressBar.Value = 0;
                    progressBar.Maximum = words.Length;
                });

                foreach (string w in words)
                {
                    Invoke((Action)delegate
                    {
                        progressBar.Value++;
                        progresStatus.Text = String.Format("Reading words... ({0} / {1})", progressBar.Value, progressBar.Maximum);
                    });

                    Regex r = new Regex("[^a-zA-ZęóąśłżźćńĘÓĄŚŁŻŹŃ]");
                    string newWord = r.Replace(w, "").ToLower();

                    if (newWord.Trim() == "")
                    {
                        continue;
                    }
                    BeginInvoke((Action)delegate
                    {
                        model.AddWord(newWord);
                    });
                }
                BeginInvoke((Action)delegate
                {
                    progresStatus.Text = "";
                    progressBar.Value = 0;
                });
            });
        }

        /* Load any Text do DataTable */
        private void loadTextFile(string fileName)
        {
            if (!File.Exists(fileName))
            {
                MessageBox.Show(
                        "Application was unable to load file: " + fileName + "! Verify file permissions.",
                        Application.ProductName,
                        MessageBoxButtons.OK, MessageBoxIcon.Error
                    );
            }

            actionQueue.QueueTask((Action)delegate
            {
                BeginInvoke((Action)delegate
                {
                    progresStatus.Text = "Loading file...";
                });
                this.processText(fileLoader.ReadAllText(fileName, model.language.expectedCharacters));
            });
        }

        private void FindWord()
        {
            actionQueue.QueueTask((Action)delegate
            {
                BeginInvoke((Action)delegate
                {
                    progresStatus.Text = "Searching for word...";
                });
                Console.Out.WriteLine("Trying to find word...");
                string[] hyphenations = model.validator.RandomHypenations();

                Invoke((Action)delegate
                {
                    foreach (string hyph in hyphenations)
                    {
                        if (!string.IsNullOrEmpty(hyph))
                        {
                            Console.Out.WriteLine("Got one!");
                            string word = hyph.Replace(" - ", "");
                            model.AddWord(word, hyph, true);
                        }
                    }
                    progresStatus.Text = "";

                    if (findWordToolStripMenuItem.Checked)
                    {
                        findersStarted++;
                        FindWord();
                    }
                    findersStarted--;
                });
            });
        }

        private void ValidateWords(List<string> words)
        {
            actionQueue.QueueTask((Action)delegate
            {
                BeginInvoke((Action)delegate
                {
                    progresStatus.Text = "Downloading data...";
                    progressBar.Value = 0;
                    progressBar.Maximum = words.Count();
                });

                Parallel.For(0, words.Count(), i =>
                {
                    BeginInvoke((Action)delegate
                    {
                        progressBar.Value++;
                        progresStatus.Text = String.Format("Downloading data... ({0} / {1})", progressBar.Value, progressBar.Maximum);
                    });
                    
                    try
                    {
                        string validationHyphenation = model.validator.getHypenation(words[i]);
                        if (!string.IsNullOrWhiteSpace(validationHyphenation))
                        {
                            model.ExternalHyphenation(words[i], validationHyphenation);
                        }
                    }
                    catch (Exception ex) { }
                });

                BeginInvoke((Action)delegate
                {
                    dataGridView1.Focus();
                    progresStatus.Text = "";
                    progressBar.Value = 0;
                });
            });
        }

        private void GenerateHyphenationForWords(IEnumerable<string> words)
        {
            actionQueue.QueueTask((Action)delegate
            {
                BeginInvoke((Action)delegate
                {
                    progresStatus.Text = "Generating data...";
                    progressBar.Value = 0;
                    progressBar.Maximum = words.Count();
                });

                foreach (string word in words)
                {
                    BeginInvoke((Action)delegate
                    {
                        progressBar.Value++;
                    });
                    model.HyphenateWord(word);
                }

                BeginInvoke((Action)delegate
                {
                    progresStatus.Text = "";
                    progressBar.Value = 0;
                });
            });
        }

        private void ShowGraphForSyllables(SyllableTransitions transitionsGraph, IEnumerable<string> syllables)
        {
            Form treeForm = new Form();
            Microsoft.Msagl.GraphViewerGdi.GViewer viewer = new Microsoft.Msagl.GraphViewerGdi.GViewer();
            Microsoft.Msagl.Drawing.Graph graph = new Microsoft.Msagl.Drawing.Graph("graph");

            foreach (KeyValuePair<string, SyllableTransitions.SyllableNode> node in transitionsGraph.transitions)
            {
                if (syllables.Contains(node.Key))
                {
                    int sum = node.Value.EdgesOutCount();
                    graph.AddNode(node.Key);
                    foreach (KeyValuePair<string, int> edgeOut in node.Value.edgesOut)
                    {
                        string percent = String.Format("{0}%", 100 * edgeOut.Value / sum);
                        graph.AddEdge(node.Key, percent, edgeOut.Key);
                    }
                    foreach (KeyValuePair<string, int> edgeIn in node.Value.edgesIn)
                    {
                        if (!syllables.Contains(edgeIn.Key))
                        {
                            int inSum = transitionsGraph.transitions[edgeIn.Key].EdgesOutCount();
                            string percent = String.Format("{0}%", 100 * edgeIn.Value / inSum);
                            graph.AddEdge(edgeIn.Key, percent, node.Key);
                        }
                    }
                }
            }

            viewer.Graph = graph;
            treeForm.SuspendLayout();
            viewer.Dock = DockStyle.Fill;
            treeForm.Controls.Add(viewer);
            treeForm.ResumeLayout();
            treeForm.ShowDialog();
        }

        /*
         * Event handlers
         */

        private void ChangeLanguageText(object sender, EventArgs e)
        {
            BeginInvoke((Action)delegate
            {
                languageStatus.Text = "Language: " + model.language.text;
            });
        }

        private void ChangeStatisticsText(object sender, EventArgs e)
        {
            BeginInvoke((Action)delegate
            {
                if (model.validationPool == 0)
                {
                    statStatus.Text = "Statistics: 0 / 0 (0%)";
                }
                else
                {
                    this.statStatus.Text = String.Format("Statistics: {0} / {1} ({2}%)",
                        model.validCount.ToString(),
                        model.validationPool.ToString(),
                        Math.Ceiling((double)(model.validCount * 100 / model.validationPool)).ToString()
                    );
                }
            });
        }

        /*
         * Main menu functions
         */

        /* Load text file */
        private void loadTextToolStripMenuItem_Click(object sender, EventArgs e)
        {
            OpenFileDialog d = new OpenFileDialog();
            d.Filter = "Text Files (.txt)|*.txt|All Files (*.*)|*.*";
            d.FilterIndex = 1;
            d.CheckFileExists = true;

            if (d.ShowDialog() == DialogResult.OK)
            {
                this.loadTextFile(d.FileName);
            }
        }

        /* Open example file handler - for dynamic loads */
        private void OpenExampleFileHandler(object sender, EventArgs e)
        {
            ToolStripMenuItem s = (ToolStripMenuItem)sender;
            string path = Application.StartupPath + @"\examples\" + s.Text + ".txt";

            this.loadTextFile(path);
        }

        /* Prompt user for text */
        private void provideTextToolStripMenuItem_Click(object sender, EventArgs e)
        {
            promptForm p = new promptForm();
            string s = p.getEnteredText();
            if (s.Trim() != "")
            {
                this.processText(s);
            }
        }

        /* Change current language */
        private void changeLanguageToolStripMenuItem_Click(object sender, EventArgs e)
        {
            model.ChangeLanguage(new langForm().selectLang());
        }

        private void closeToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        /* Run validation */
        private void validateOnlineToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (this.dataGridView1.Rows.Count == 0)
            {
                MessageBox.Show(
                        "There are no words on list to validate!",
                        Application.ProductName,
                        MessageBoxButtons.OK, MessageBoxIcon.Error
                    );
                return;
            }
            ValidateWords(model.dataTable.AsEnumerable().Select(r => r.Field<string>("Word")).ToList());
        }

        private void ValidateSelected_Click(object sender, EventArgs e)
        {
            List<string> words = new List<string>();
            foreach (DataGridViewRow row in dataGridView1.SelectedRows)
            {
                words.Add(row.Cells[HyphenationModel.wordColumn].Value.ToString());
            }
            ValidateWords(words);
        }

        /* Calculate syllables menu */
        private void generateToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (this.dataGridView1.Rows.Count == 0)
            {
                MessageBox.Show(
                        "There are no words on list to validate!",
                        Application.ProductName,
                        MessageBoxButtons.OK, MessageBoxIcon.Error
                    );
                return;
            }

            GenerateHyphenationForWords(model.dataTable.AsEnumerable().Select(r => r.Field<string>("Word")).ToArray());
        }

        private void GenerateFromSelected_Click(object sender, EventArgs e)
        {
            List<string> words = new List<string>();
            foreach (DataGridViewRow row in dataGridView1.SelectedRows)
            {
                words.Add(row.Cells[HyphenationModel.wordColumn].Value.ToString());
            }
            GenerateHyphenationForWords(words);
        }

        private void dataGridView1_CellFormatting(object sender, DataGridViewCellFormattingEventArgs e)
        {
            if (e.ColumnIndex == dataGridView1.Columns[HyphenationModel.syllableColumn].Index || 
                e.ColumnIndex == dataGridView1.Columns[HyphenationModel.dictionaryColumn].Index)
            {
                int i = e.RowIndex;
                var row = this.dataGridView1.Rows[i];

                if (!model.IsRowCounted(row))
                {
                    dataGridView1.Rows[i].DefaultCellStyle.BackColor = Color.White;
                }
                else if (model.IsRowValid(row))
                {
                    dataGridView1.Rows[i].DefaultCellStyle.BackColor = Color.FromArgb(190, 220, 165);
                }
                else
                {
                    dataGridView1.Rows[i].DefaultCellStyle.BackColor = Color.FromArgb(220, 190, 190);
                }
            }
        }

        private void clearWordsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("Do you really want to clear all words?", "Are you sure?",
                MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2) == DialogResult.Yes)
            {
                model.ClearAll();
            }
        }

        private void findWordToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (findersStarted == 0)
            {
                findersStarted++;
                FindWord();
            }
        }

        private void saveWordsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (this.dataGridView1.Rows.Count == 0)
            {
                MessageBox.Show(
                        "There are no words on list to save!",
                        Application.ProductName,
                        MessageBoxButtons.OK, MessageBoxIcon.Error
                    );
                return;
            }

            SaveFileDialog saveFileDialog = new SaveFileDialog();

            saveFileDialog.Filter = "txt files (*.txt)|*.txt|All files (*.*)|*.*";
            saveFileDialog.FilterIndex = 1;
            saveFileDialog.RestoreDirectory = true;

            if (saveFileDialog.ShowDialog() == DialogResult.OK)
            {
                Stream fileSaveStream;
                if ((fileSaveStream = saveFileDialog.OpenFile()) != null)
                {
                    StreamWriter fileWriter = new StreamWriter(fileSaveStream);
                    foreach (DataRow row in model.dataTable.Rows)
                    {
                        string hyphenation = row[HyphenationModel.dictionaryColumn].ToString();
                        if (!string.IsNullOrEmpty(hyphenation))
                        {
                            fileWriter.WriteLine(hyphenation.Replace(" ", ""));
                        }
                    }
                    fileWriter.Flush();
                    fileWriter.Close();
                    fileSaveStream.Close();
                }
            }
        }

        private void loadWordsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            OpenFileDialog openFileDialog = new OpenFileDialog();

            openFileDialog.Filter = "txt files (*.txt)|*.txt|All files (*.*)|*.*";
            openFileDialog.FilterIndex = 1;
            openFileDialog.RestoreDirectory = true;

            if (openFileDialog.ShowDialog() == DialogResult.OK)
            {
                Stream fileLoadStream;
                if ((fileLoadStream = openFileDialog.OpenFile()) != null)
                {
                    StreamReader fileReader = new StreamReader(fileLoadStream);
                    string line;
                    while ((line = fileReader.ReadLine()) != null)
                    {
                        string word = line.Replace(" ", "").Replace("-", "");
                        model.AddWord(word, line.Replace("-", " - "));
                    }
                    fileReader.Close();
                    fileLoadStream.Close();
                }
            }
        }

        private void drawGraphToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (this.dataGridView1.Rows.Count == 0)
            {
                MessageBox.Show(
                        "There are no words on list to build a tree!",
                        Application.ProductName,
                        MessageBoxButtons.OK, MessageBoxIcon.Error
                    );
                return;
            }

            var words = model.dataTable.AsEnumerable().Select(r => r.Field<string>("Syllable"));
            SyllableTransitions transitionsGraph = new SyllableTransitions(words);

            if (transitionsGraph.transitions.Count == 0)
            {
                MessageBox.Show(
                        "There are no words hyphenated on list to build a tree!",
                        Application.ProductName,
                        MessageBoxButtons.OK, MessageBoxIcon.Error
                    );
                return;
            }

            ShowGraphForSyllables(transitionsGraph, transitionsGraph.transitions.Keys);
        }

        private void GraphFromSelected_Click(object sender, EventArgs e)
        {
            var words = model.dataTable.AsEnumerable().Select(r => r.Field<string>("Syllable"));
            SyllableTransitions transitionsGraph = new SyllableTransitions(words);
            if (transitionsGraph.transitions.Count > 0)
            {
                HashSet<string> syllables = new HashSet<string>();
                foreach (DataGridViewRow row in dataGridView1.SelectedRows)
                {
                    string[] wordSyllables = row.Cells[HyphenationModel.syllableColumn].Value.ToString()
                        .Replace(" ", "").Split("-".ToCharArray(), StringSplitOptions.RemoveEmptyEntries);
                    foreach (string syllable in wordSyllables)
                    {
                        syllables.Add(syllable);
                    }
                }
                ShowGraphForSyllables(transitionsGraph, syllables);
            }
        }

        private void encodingSelected(object sender, EventArgs e)
        {
            ToolStripMenuItem clickedItem = sender as ToolStripMenuItem;
            if (clickedItem.Equals(moreToolStripMenuItem))
            {
                EncodingChooser chooser = new EncodingChooser(fileLoader.encoding);
                Encoding encoding = chooser.GetChoosenEncoding();
                if (encoding != null)
                {
                    fileLoader.encoding = encoding;
                }
            }
            if (encodingMenuMapping.ContainsKey(clickedItem))
            {
                fileLoader.encoding = encodingMenuMapping[clickedItem];
            }

            foreach (ToolStripMenuItem siblingItem in clickedItem.Owner.Items)
            {
                siblingItem.Checked = false;
            }
            if (encodingMenuMapping.ContainsValue(fileLoader.encoding))
            {
                foreach (KeyValuePair<ToolStripMenuItem, Encoding> item2enc in encodingMenuMapping)
                {
                    if (fileLoader.encoding == item2enc.Value)
                    {
                        item2enc.Key.Checked = true;
                    }
                }
            }
            else
            {
                moreToolStripMenuItem.Checked = true;
            }

        }

        private void loadSampleTextToolStripMenuItem_DropDownOpening(object sender, EventArgs e)
        {
            ReloadSampleTextMenu();
        }

        private void authorsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            MessageBox.Show("Maria Kucułyma, Daniel Kossakowski, Przemysław Czuj");
        }

        private void showAnalyzerToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (this.dataGridView1.Rows.Count == 0)
            {
                MessageBox.Show(
                        "There are no words on list to build a tree!",
                        Application.ProductName,
                        MessageBoxButtons.OK, MessageBoxIcon.Error
                    );
                return;
            }

            analyzerForm a = new analyzerForm();
            a.runAnalyzer(model);
        }

        private void dataGridView1_DataError(object sender, DataGridViewDataErrorEventArgs e)
        {
            // do nothing
        }

        private void MarkAsValidSelected_Click(object sender, EventArgs e)
        {
            foreach (DataGridViewRow row in dataGridView1.SelectedRows)
            {
                model.ExternalHyphenation(row.Cells[HyphenationModel.wordColumn].Value.ToString(),
                    row.Cells[HyphenationModel.syllableColumn].Value.ToString());
            }
        }

        private void dataGridView1_CellValidating(object sender, DataGridViewCellValidatingEventArgs e)
        {
            if (e.ColumnIndex == dataGridView1.Columns[HyphenationModel.dictionaryColumn].Index)
            {
                string word = dataGridView1[HyphenationModel.wordColumn, e.RowIndex].Value.ToString();
                model.ExternalHyphenation(word, e.FormattedValue.ToString());
            }
        }
    }

    public class HyphenationModel
    {
        public DataTable dataTable;
        public HyphenationLanguage language;
        public Hyphenator hypenator;
        public Validator validator;

        public event EventHandler lanugageChanged;
        public event EventHandler statisticsChanged;

        public int validationPool = 0;
        public int validCount = 0;

        public const string wordColumn = "Word";
        public const string countColumn = "Count";
        public const string syllableColumn = "Syllable";
        public const string dictionaryColumn = "Syllable (dictionary)";

        public HyphenationModel()
        {
            dataTable = new DataTable();
            dataTable.Columns.Add(wordColumn);
            dataTable.Columns.Add(countColumn, typeof(int));
            dataTable.Columns.Add(syllableColumn);
            dataTable.Columns.Add(dictionaryColumn);

            dataTable.PrimaryKey = new DataColumn[] { dataTable.Columns[wordColumn] };
        }

        public void AddWord(string word, string externalHyphenation = "", bool hyphenate = false)
        {
            var row = this.dataTable.Rows.Find(word);
            if (row == null)
            {
                this.dataTable.Rows.Add(word, 1);
            }
            else
            {
                row[HyphenationModel.countColumn] = Convert.ToInt32(row[HyphenationModel.countColumn].ToString()) + 1;
            }
            ExternalHyphenation(word, externalHyphenation);
            if (hyphenate)
            {
                HyphenateWord(word);
            }
        }

        public void ExternalHyphenation(string word, string hyph)
        {
            DataRow row = dataTable.Rows.Find(word);
            if (row != null)
            {
                CountStatistics(row, (Action)delegate
                {
                    row[HyphenationModel.dictionaryColumn] = hyph;
                });
            }
        }

        public void ChangeLanguage(HyphenationLanguage language)
        {
            this.language = language;
            this.validator = new Validator(this.language);

            if (!File.Exists(this.language.patternFilePath))
            {
                MessageBox.Show(
                        "Could not open pattern file at: " + this.language.patternFilePath,
                        Application.ProductName,
                        MessageBoxButtons.OK, MessageBoxIcon.Error
                    );
            }
            else
            {
                string patternString = File.ReadAllText(this.language.patternFilePath);
                string exceptionsString = File.Exists(this.language.exceptionsFilePath) ? File.ReadAllText(this.language.exceptionsFilePath) : "";

                this.hypenator = new Hyphenator(patternString, exceptionsString, this.language.minLeft, this.language.minRight);
            }

            if (lanugageChanged != null)
            {
                lanugageChanged(this, EventArgs.Empty);
            }
        }

        public void HyphenateWord(string word)
        {
            DataRow row = this.dataTable.Rows.Find(word);

            CountStatistics(row, (Action)delegate
            {
                row[HyphenationModel.syllableColumn] = hypenator.HyphenateWord(word, " - ");
            });
        }

        public void CountStatistics(DataRow row, Action action)
        {
            bool wasCounted = IsRowCounted(row);
            bool wasValid = IsRowValid(row);

            action();

            bool isNowValid = IsRowValid(row);
            bool isNowCounted = IsRowCounted(row);

            bool changed = false;

            if (!wasCounted && isNowCounted)
            {
                changed = true; validationPool++;
                if (isNowValid)
                {
                    validCount++;
                }
            }
            else if (wasCounted && !isNowCounted)
            {
                changed = true; validationPool--;
                if (wasValid)
                {
                    validCount--;
                }
            }
            else if (!wasValid && isNowValid)
            {
                changed = true; validCount++;
            }
            else if (wasValid && !isNowValid)
            {
                changed = true; validCount--;
            }

            if (changed && statisticsChanged != null)
            {
                statisticsChanged(this, EventArgs.Empty);
            }
        }

        public bool IsRowCounted(DataGridViewRow gridRow)
        {
            return IsRowCounted(((DataRowView)gridRow.DataBoundItem).Row);
        }

        public bool IsRowValid(DataGridViewRow gridRow)
        {
            return IsRowValid(((DataRowView)gridRow.DataBoundItem).Row);
        }

        public bool IsRowValid(DataRow row)
        {
            string theirs = row[HyphenationModel.dictionaryColumn].ToString();
            string our = row[HyphenationModel.syllableColumn].ToString();
            return our.StartsWith(theirs) || theirs.StartsWith(our);
        }

        public bool IsRowCounted(DataRow row)
        {
            string theirs = row[HyphenationModel.dictionaryColumn].ToString();
            string our = row[HyphenationModel.syllableColumn].ToString();
            return !string.IsNullOrWhiteSpace(our) && !string.IsNullOrWhiteSpace(theirs);
        }

        public void ClearAll()
        {
            dataTable.Clear();
            validationPool = 0;
            validCount = 0;
            statisticsChanged(this, EventArgs.Empty);
        }
    }

    public class SyllableTransitions
    {
        public Dictionary<string, SyllableNode> transitions = new Dictionary<string, SyllableNode>();

        public SyllableTransitions(IEnumerable<string> words)
        {
            foreach (string word in words)
            {
                if (word == null)
                {
                    continue;
                }
                string[] syllables = word.Replace(" ", "").Split("-".ToCharArray(), StringSplitOptions.RemoveEmptyEntries);
                if (syllables.Length == 1)
                {
                    AssurePresent(syllables[0]);
                }
                for (int i = 0; i < syllables.Length - 1; i++)
                {
                    AssurePresent(syllables[i]);
                    AssurePresent(syllables[i + 1]);
                    SyllableNode.AddEdge(transitions[syllables[i]], transitions[syllables[i + 1]]);
                }
            }
        }

        private void AssurePresent(string syllable)
        {
            if (!transitions.ContainsKey(syllable))
            {
                transitions[syllable] = new SyllableNode(syllable);
            }
        }

        public class SyllableNode
        {
            public readonly string text;
            public Dictionary<string, int> edgesOut = new Dictionary<string, int>();
            public Dictionary<string, int> edgesIn = new Dictionary<string, int>();

            public SyllableNode(string text)
            {
                this.text = text;
            }

            public int EdgesOutCount()
            {
                return edgesOut.AsEnumerable().Sum(edge => edge.Value);
            }

            public static void AddEdge(SyllableNode nodeA, SyllableNode nodeB)
            {
                nodeA.edgesOut[nodeB.text] = nodeA.edgesOut.ContainsKey(nodeB.text) ? nodeA.edgesOut[nodeB.text] + 1 : 1;
                nodeB.edgesIn[nodeA.text] = nodeB.edgesIn.ContainsKey(nodeA.text) ? nodeB.edgesIn[nodeA.text] + 1 : 1;
            }
        }
    }

    public class ActionQueue
    {
        private Task previousTask = Task.FromResult(true);
        private object key = new object();

        public Task QueueTask(Action action)
        {
            lock (key)
            {
                previousTask = previousTask.ContinueWith(
                    t => { try { action(); }
                        catch (Exception ex) {
                            MessageBox.Show("An unexpected error occured in task thread. We are sorry :(");
                        }
                    }, 
                    System.Threading.CancellationToken.None, 
                    TaskContinuationOptions.None, 
                    TaskScheduler.Default);
                return previousTask;
            }
        }
    }

    public class FileLoader
    {
        public Encoding encoding;

        public string ReadAllText(string path, string expectedCharacters = "A-Za-z")
        {
            if (encoding == null)
            {
                return SmartReadAllText(path, expectedCharacters);
            }
            else
            {
                return File.ReadAllText(path, encoding);
            }
        }

        public static string SmartReadAllText(string filepath, string expectedCharacters)
        {
            List<string> encodedTexts = new List<string>();
            encodedTexts.Add(File.ReadAllText(filepath));
            encodedTexts.Add(File.ReadAllText(filepath, Encoding.Default));
            try { encodedTexts.Add(File.ReadAllText(filepath, Encoding.GetEncoding("iso-8859-2"))); } catch(Exception ex) { }

            return encodedTexts.AsEnumerable().OrderByDescending(text => Regex.Matches(text, "[" + expectedCharacters + "]").Count).First();
        }
    }
}
