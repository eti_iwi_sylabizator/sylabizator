﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace sylabizator
{
    public partial class promptForm : Form
    {
        public promptForm()
        {
            InitializeComponent();
        }

        public string getEnteredText()
        {
            this.ShowDialog();
            comboBox1.Focus();

            return comboBox1.Text;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
