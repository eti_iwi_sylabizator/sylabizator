﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace sylabizator
{
    public partial class langForm : Form
    {
        HyphenationLanguage selectedLang = HyphenationLanguage.ENGLISH;

        public langForm()
        {
            InitializeComponent();
        }

        public HyphenationLanguage selectLang()
        {
            if(Properties.Settings.Default.selectedLang != "")
            {
                this.rememberCheckBox.Checked = true;
            }

            this.ShowDialog();

            /* Save settings - if user checked CheckBox */
            if (!this.rememberCheckBox.Checked)
            {
                HyphenationLanguage.StoreInProperties("");
            }
            else
            {
                this.selectedLang.StoreInProperties();
            }

            /* Return value to mainForm */
            return this.selectedLang;
        }

        private void polButton_Click(object sender, EventArgs e)
        {
            this.selectedLang = HyphenationLanguage.POLISH;
            this.Close();
        }

        private void engButton_Click(object sender, EventArgs e)
        {
            this.selectedLang = HyphenationLanguage.ENGLISH;
            this.Close();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            OpenFileDialog patternFileDialog = new OpenFileDialog();
            patternFileDialog.Title = "Choose pattern file";
            patternFileDialog.InitialDirectory = HyphenationLanguage.PATTERN_DIR;
            patternFileDialog.Filter = "Pattern Files (.pat.txt)|*.pat.txt|All Files (*.*)|*.*";
            patternFileDialog.FilterIndex = 1;

            if (patternFileDialog.ShowDialog() == DialogResult.OK)
            {
                OpenFileDialog exceptionsFileDialog = new OpenFileDialog();
                exceptionsFileDialog.Title = "Choose exceptions file (optional)";
                exceptionsFileDialog.InitialDirectory = HyphenationLanguage.PATTERN_DIR;
                exceptionsFileDialog.Filter = "Exception Files (.hyp.txt)|*.hyp.txt|All Files (*.*)|*.*";
                exceptionsFileDialog.FilterIndex = 1;
                exceptionsFileDialog.ShowDialog();

                this.selectedLang = new HyphenationLanguage(patternFileDialog.FileName, exceptionsFileDialog.FileName);
                this.Close();
            }
        }
    }

    public class HyphenationLanguage
    {
        public static readonly string PATTERN_DIR = Application.StartupPath + @"\rules\";

        public static readonly HyphenationLanguage POLISH =
            new HyphenationLanguage(PATTERN_DIR + "hyph-pl.pat.txt", PATTERN_DIR + "hyph-pl.hyp.txt", 1, 1, "Polish", "POL", "a-zA-ZęóąśłżźćńĘÓĄŚŁŻŹŃ", "pl");

        public static readonly HyphenationLanguage ENGLISH_GB =
            new HyphenationLanguage(PATTERN_DIR + "hyph-en-gb.pat.txt", PATTERN_DIR + "hyph-en-gb.hyp.txt", 1, 2, "English", "ENG", "a-zA-Z", "en");

        public static readonly HyphenationLanguage ENGLISH_US =
            new HyphenationLanguage(PATTERN_DIR + "hyph-en-us.pat.txt", PATTERN_DIR + "hyph-en-us.hyp.txt", 1, 2, "English", "ENG", "a-zA-Z", "en");

        public static readonly HyphenationLanguage ENGLISH = ENGLISH_US;

        public static readonly Dictionary<string, HyphenationLanguage> LANG_PROPERTIES = new Dictionary<string, HyphenationLanguage>
        {
            { "", ENGLISH },
            { ENGLISH.propertyText, ENGLISH },
            { POLISH.propertyText, POLISH }
        };

        public string patternFilePath;
        public string exceptionsFilePath;
        public int minLeft;
        public int minRight;
        public string text;
        public string propertyText;
        public string expectedCharacters;

        public string domain;

        public HyphenationLanguage(string patternFile, string exceptionsFilePath,
            int minLeft = 1, int minRight = 1,
            string text = "Custom", 
            string propertyText = "",
            string expectedCharacters = "a-zA-ZęóąśłżźćńĘÓĄŚŁŻŹŃ", 
            string domain = "en")
        {
            this.patternFilePath = patternFile;
            this.exceptionsFilePath = exceptionsFilePath;
            this.minLeft = minLeft;
            this.minRight = minRight;
            this.text = text;
            this.propertyText = propertyText;
            this.expectedCharacters = expectedCharacters;
            this.domain = domain;
        }

        public string WiktionaryBaseRegexp()
        {
            return @"\{\{hyphenation\|([" + expectedCharacters + @"|]*?)\|lang=en\}\}";
        }

        public void StoreInProperties()
        {
            StoreInProperties(this.propertyText);
        }

        public static void StoreInProperties(string propertyText)
        {
            Properties.Settings.Default.selectedLang = propertyText;
            Properties.Settings.Default.Save();
        }

        public static HyphenationLanguage LoadFromProperties()
        {
            return LANG_PROPERTIES[Properties.Settings.Default.selectedLang];
        }

        internal static bool IsSaved()
        {
            return Properties.Settings.Default.selectedLang != "";
        }
    }
}
