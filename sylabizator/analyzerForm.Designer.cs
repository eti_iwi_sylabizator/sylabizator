﻿namespace sylabizator
{
    partial class analyzerForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.headerPanel = new System.Windows.Forms.Panel();
            this.headerLabel = new System.Windows.Forms.Label();
            this.prevGridView = new System.Windows.Forms.DataGridView();
            this.prevColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.wordsGridView = new System.Windows.Forms.DataGridView();
            this.nextGridView = new System.Windows.Forms.DataGridView();
            this.nextCol = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.filterBox = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.headerPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.prevGridView)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.wordsGridView)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nextGridView)).BeginInit();
            this.SuspendLayout();
            // 
            // headerPanel
            // 
            this.headerPanel.BackColor = System.Drawing.Color.LightGray;
            this.headerPanel.Controls.Add(this.headerLabel);
            this.headerPanel.Dock = System.Windows.Forms.DockStyle.Top;
            this.headerPanel.ForeColor = System.Drawing.SystemColors.ControlText;
            this.headerPanel.Location = new System.Drawing.Point(0, 0);
            this.headerPanel.Name = "headerPanel";
            this.headerPanel.Size = new System.Drawing.Size(559, 31);
            this.headerPanel.TabIndex = 9;
            // 
            // headerLabel
            // 
            this.headerLabel.AutoSize = true;
            this.headerLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.headerLabel.Location = new System.Drawing.Point(12, 9);
            this.headerLabel.Name = "headerLabel";
            this.headerLabel.Size = new System.Drawing.Size(88, 13);
            this.headerLabel.TabIndex = 0;
            this.headerLabel.Text = "Text providing";
            // 
            // prevGridView
            // 
            this.prevGridView.AllowUserToAddRows = false;
            this.prevGridView.AllowUserToDeleteRows = false;
            this.prevGridView.AllowUserToResizeRows = false;
            this.prevGridView.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.prevGridView.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.Single;
            this.prevGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
            this.prevGridView.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.prevColumn});
            this.prevGridView.Location = new System.Drawing.Point(14, 60);
            this.prevGridView.MultiSelect = false;
            this.prevGridView.Name = "prevGridView";
            this.prevGridView.ReadOnly = true;
            this.prevGridView.RowHeadersVisible = false;
            this.prevGridView.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.prevGridView.Size = new System.Drawing.Size(151, 216);
            this.prevGridView.TabIndex = 10;
            this.prevGridView.CellDoubleClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.prevGridView_CellDoubleClick);
            // 
            // prevColumn
            // 
            this.prevColumn.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.prevColumn.HeaderText = "Previous syllable";
            this.prevColumn.Name = "prevColumn";
            this.prevColumn.ReadOnly = true;
            // 
            // wordsGridView
            // 
            this.wordsGridView.AllowUserToAddRows = false;
            this.wordsGridView.AllowUserToDeleteRows = false;
            this.wordsGridView.AllowUserToResizeRows = false;
            this.wordsGridView.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.wordsGridView.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.Single;
            this.wordsGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
            this.wordsGridView.Location = new System.Drawing.Point(202, 86);
            this.wordsGridView.MultiSelect = false;
            this.wordsGridView.Name = "wordsGridView";
            this.wordsGridView.ReadOnly = true;
            this.wordsGridView.RowHeadersVisible = false;
            this.wordsGridView.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.wordsGridView.Size = new System.Drawing.Size(151, 204);
            this.wordsGridView.TabIndex = 11;
            this.wordsGridView.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.wordsGridView_CellClick);
            // 
            // nextGridView
            // 
            this.nextGridView.AllowUserToAddRows = false;
            this.nextGridView.AllowUserToDeleteRows = false;
            this.nextGridView.AllowUserToResizeRows = false;
            this.nextGridView.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.nextGridView.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.Single;
            this.nextGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
            this.nextGridView.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.nextCol});
            this.nextGridView.Location = new System.Drawing.Point(389, 60);
            this.nextGridView.MultiSelect = false;
            this.nextGridView.Name = "nextGridView";
            this.nextGridView.ReadOnly = true;
            this.nextGridView.RowHeadersVisible = false;
            this.nextGridView.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.nextGridView.Size = new System.Drawing.Size(153, 216);
            this.nextGridView.TabIndex = 12;
            this.nextGridView.CellDoubleClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.nextGridView_CellDoubleClick);
            // 
            // nextCol
            // 
            this.nextCol.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.nextCol.HeaderText = "Next syllable";
            this.nextCol.Name = "nextCol";
            this.nextCol.ReadOnly = true;
            // 
            // filterBox
            // 
            this.filterBox.Location = new System.Drawing.Point(202, 60);
            this.filterBox.Name = "filterBox";
            this.filterBox.Size = new System.Drawing.Size(151, 20);
            this.filterBox.TabIndex = 13;
            this.filterBox.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.filterBox_KeyPress);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 44);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(85, 13);
            this.label1.TabIndex = 14;
            this.label1.Text = "Previous syllable";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(386, 44);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(66, 13);
            this.label2.TabIndex = 15;
            this.label2.Text = "Next syllable";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(199, 44);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(32, 13);
            this.label3.TabIndex = 16;
            this.label3.Text = "Filter:";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label4.Location = new System.Drawing.Point(11, 280);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(135, 13);
            this.label4.TabIndex = 17;
            this.label4.Text = "(Use double click to select)";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label5.Location = new System.Drawing.Point(386, 280);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(135, 13);
            this.label5.TabIndex = 18;
            this.label5.Text = "(Use double click to select)";
            // 
            // analyzerForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(559, 302);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.filterBox);
            this.Controls.Add(this.nextGridView);
            this.Controls.Add(this.wordsGridView);
            this.Controls.Add(this.prevGridView);
            this.Controls.Add(this.headerPanel);
            this.Name = "analyzerForm";
            this.Text = "analyzerForm";
            this.headerPanel.ResumeLayout(false);
            this.headerPanel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.prevGridView)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.wordsGridView)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nextGridView)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Panel headerPanel;
        private System.Windows.Forms.Label headerLabel;
        private System.Windows.Forms.DataGridView prevGridView;
        private System.Windows.Forms.DataGridView wordsGridView;
        private System.Windows.Forms.DataGridView nextGridView;
        private System.Windows.Forms.TextBox filterBox;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.DataGridViewTextBoxColumn prevColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn nextCol;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
    }
}