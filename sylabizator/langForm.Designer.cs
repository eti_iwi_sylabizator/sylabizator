﻿namespace sylabizator
{
    partial class langForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.polButton = new System.Windows.Forms.Button();
            this.engButton = new System.Windows.Forms.Button();
            this.headerPanel = new System.Windows.Forms.Panel();
            this.headerLabel = new System.Windows.Forms.Label();
            this.rememberCheckBox = new System.Windows.Forms.CheckBox();
            this.button1 = new System.Windows.Forms.Button();
            this.headerPanel.SuspendLayout();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(20, 45);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(110, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Select your language:";
            // 
            // polButton
            // 
            this.polButton.Cursor = System.Windows.Forms.Cursors.Default;
            this.polButton.FlatAppearance.BorderSize = 0;
            this.polButton.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.polButton.Image = global::sylabizator.Properties.Resources.flag_pl;
            this.polButton.Location = new System.Drawing.Point(35, 71);
            this.polButton.Name = "polButton";
            this.polButton.Size = new System.Drawing.Size(57, 37);
            this.polButton.TabIndex = 1;
            this.polButton.UseVisualStyleBackColor = true;
            this.polButton.Click += new System.EventHandler(this.polButton_Click);
            // 
            // engButton
            // 
            this.engButton.Cursor = System.Windows.Forms.Cursors.Default;
            this.engButton.FlatAppearance.BorderSize = 0;
            this.engButton.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.engButton.Image = global::sylabizator.Properties.Resources.flag_en;
            this.engButton.Location = new System.Drawing.Point(160, 71);
            this.engButton.Name = "engButton";
            this.engButton.Size = new System.Drawing.Size(57, 37);
            this.engButton.TabIndex = 2;
            this.engButton.UseVisualStyleBackColor = true;
            this.engButton.Click += new System.EventHandler(this.engButton_Click);
            // 
            // headerPanel
            // 
            this.headerPanel.BackColor = System.Drawing.Color.LightGray;
            this.headerPanel.Controls.Add(this.headerLabel);
            this.headerPanel.Dock = System.Windows.Forms.DockStyle.Top;
            this.headerPanel.ForeColor = System.Drawing.SystemColors.ControlText;
            this.headerPanel.Location = new System.Drawing.Point(0, 0);
            this.headerPanel.Name = "headerPanel";
            this.headerPanel.Size = new System.Drawing.Size(250, 31);
            this.headerPanel.TabIndex = 3;
            // 
            // headerLabel
            // 
            this.headerLabel.AutoSize = true;
            this.headerLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.headerLabel.Location = new System.Drawing.Point(12, 9);
            this.headerLabel.Name = "headerLabel";
            this.headerLabel.Size = new System.Drawing.Size(118, 13);
            this.headerLabel.TabIndex = 0;
            this.headerLabel.Text = "Language selection";
            // 
            // rememberCheckBox
            // 
            this.rememberCheckBox.AutoSize = true;
            this.rememberCheckBox.Location = new System.Drawing.Point(35, 123);
            this.rememberCheckBox.Name = "rememberCheckBox";
            this.rememberCheckBox.Size = new System.Drawing.Size(127, 17);
            this.rememberCheckBox.TabIndex = 1;
            this.rememberCheckBox.Text = "Remember my choise";
            this.rememberCheckBox.UseVisualStyleBackColor = true;
            // 
            // button1
            // 
            this.button1.Font = new System.Drawing.Font("Microsoft Sans Serif", 6F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.button1.Location = new System.Drawing.Point(168, 123);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(49, 17);
            this.button1.TabIndex = 4;
            this.button1.Text = "From File";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // langForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(250, 156);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.rememberCheckBox);
            this.Controls.Add(this.headerPanel);
            this.Controls.Add(this.engButton);
            this.Controls.Add(this.polButton);
            this.Controls.Add(this.label1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "langForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Language";
            this.headerPanel.ResumeLayout(false);
            this.headerPanel.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button polButton;
        private System.Windows.Forms.Button engButton;
        private System.Windows.Forms.Panel headerPanel;
        private System.Windows.Forms.Label headerLabel;
        private System.Windows.Forms.CheckBox rememberCheckBox;
        private System.Windows.Forms.Button button1;
    }
}