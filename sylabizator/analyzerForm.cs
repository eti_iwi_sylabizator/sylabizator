﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace sylabizator
{
    public partial class analyzerForm : Form
    {
        public analyzerForm()
        {
            InitializeComponent();
        }

        private DataTable dataTable;
        private HyphenationModel model;

        public void runAnalyzer(HyphenationModel m)
        {
            this.model = m;
            dataTable = new DataTable();
            dataTable.Columns.Add("Syllables");
            dataTable.PrimaryKey = new DataColumn[] { dataTable.Columns[0] };
            wordsGridView.DataSource = dataTable;
            wordsGridView.Columns[0].AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;

            var words = model.dataTable.AsEnumerable().Select(r => r.Field<string>("Syllable"));

            foreach(string word in words)
            {
                if (word == null)
                {
                    continue;
                }
                string[] wordSyllables = word.Replace(" ", "").Split("-".ToCharArray(), StringSplitOptions.RemoveEmptyEntries);
                foreach (string syllable in wordSyllables)
                {
                    var row = this.dataTable.Rows.Find(syllable);
                    if (row == null)
                    {
                        this.dataTable.Rows.Add(syllable);
                    }
                }
            }
            this.Show();
        }

        private void wordsGridView_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            prevGridView.Rows.Clear();
            nextGridView.Rows.Clear();
            string selectedSyllable = wordsGridView.SelectedRows[0].Cells[0].Value.ToString();

            var words = model.dataTable.AsEnumerable().Select(r => r.Field<string>("Syllable"));
            SyllableTransitions sTrans = new SyllableTransitions(words);
            foreach (KeyValuePair<string, SyllableTransitions.SyllableNode> node in sTrans.transitions)
            {
                foreach (KeyValuePair<string, int> edgeOut in node.Value.edgesOut)
                {
                    if(selectedSyllable == node.Key)
                    {
                        nextGridView.Rows.Add(edgeOut.Key);
                    }
                }
                foreach (KeyValuePair<string, int> edgeIn in node.Value.edgesIn)
                {
                    if (selectedSyllable == node.Key)
                    {
                        prevGridView.Rows.Add(edgeIn.Key);
                    }
                }
            }
        }

        private void loadSyllable(string filter)
        {
            this.dataTable.Clear();
            prevGridView.Rows.Clear();
            nextGridView.Rows.Clear();


            var words = model.dataTable.AsEnumerable().Select(r => r.Field<string>("Syllable"));
            foreach (string word in words)
            {
                if (word == null)
                {
                    continue;
                }

                string[] wordSyllables = word.Replace(" ", "").Split("-".ToCharArray(), StringSplitOptions.RemoveEmptyEntries);
                foreach (string syllable in wordSyllables)
                {
                    var row = this.dataTable.Rows.Find(syllable);
                    if (syllable.Contains(filter) && row == null)
                    {
                        this.dataTable.Rows.Add(syllable);
                    }
                }
            }
        }

        private void filterBox_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar != (char)13)
            {
                return;
            }

            loadSyllable(filterBox.Text);
        }

        private void nextGridView_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            if (nextGridView.SelectedRows.Count != 1)
            {
                return;
            }

            filterBox.Text = nextGridView.SelectedRows[0].Cells[0].Value.ToString();
            loadSyllable(filterBox.Text);
        }

        private void prevGridView_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            if (prevGridView.SelectedRows.Count != 1)
            {
                return;
            }

            filterBox.Text = prevGridView.SelectedRows[0].Cells[0].Value.ToString();
            loadSyllable(filterBox.Text);
        }
    }
}
