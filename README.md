# POLECENIE #

Utworzyć narzędzie do dzielenia wyrazów na sylaby dla języka PL/EN. Wymagana jest wysoka jakość podziału rzędu 95%.

Dla poprawy jakości zbudowanych reguł można użyć przykładami hyphenation z wikitionary.

Wykorzystując utworzone narzędzie zbudować sieć prawdopodobieństw przejść między sylabami.

Projekt proszę implementować w C#, mają działać na 64b Windows 7.

Proszę wykazać się kreatywnością. Nie pytać się jak coś zrobić tylko zaproponować do konsultacji własne, PRZEMYŚLANE rozwiązanie.

Polecam się przyłożyć do zadań - projekty mają być dopracowane.

Osoby zainteresowane konsultacjami mogą podejść do mnie w poniedziałek o 12.

Termin oddania - początek semestru jeśli dziekanat zgodzi się przedłużyć termin wprowadzenia ocen do protokołów.